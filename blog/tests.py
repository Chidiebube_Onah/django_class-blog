from django.contrib.auth import get_user_model
from django.test import Client, TestCase
from django.urls import reverse

from .models import Post

# Create your tests here.

class BlogTest(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create_user(
            username='testuser',
            email='test@email.com',
            password='secret'
        )
        self.post = Post.objects.create(
            title = 'A Good title',
            body = 'A nice body',
            author = self.user,

        )

    def test_string_rep(self):
        post = Post(title='A sample title')
        self.assertEqual(str(post), post.title)

    def test_get_absolute_url(self):
        self.assertEqual(self.post.get_absolute_url(), '/post/1/')

    def test_post_content(self):
        self.assertEqual(f'{self.post.title}', 'A Good title')
        self.assertEqual(f'{self.post.author}','testuser' )
        self.assertEqual(f'{self.post.body}', 'A nice body')

    def test_post_list_view(self):
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'A nice body')
        self.assertTemplateUsed(response, 'home.html')

    def test_post_detail_view(self):
        response = self.client.get('/post/1/')
        no_response = self.client.get('/post/1000000/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(no_response.status_code, 404)
        self.assertContains(response, 'A Good title')
        self.assertTemplateUsed(response, 'post_detail.html')

    def test_post_create_view(self):
        response = self.client.post(reverse('new_post'),{
            'title': 'New Post',
            'body': 'New Content',
            'author': self.user

        })
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'New Post')
        self.assertContains(response, 'New Content')


    def test_post_update_view(self):
        response = self.client.post(reverse('edit_post', args='1'), {
            'title': 'Posts Title',
            'body' : 'Contents'
        })
        self.assertEqual(response.status_code, 302)


    def test_post_delete_view(self):
        response = self.client.get(
            reverse('delete_post', args='1')
        )
        self.assertEqual(response.status_code, 200)
